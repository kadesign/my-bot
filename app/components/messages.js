const {botCommands} = require('../config');

module.exports = {
  commands: {
    help: '*Команды:*\n\n' +
      '/' + botCommands.help + ' - список команд'
  }
};