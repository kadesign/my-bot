const Logger = require('../utils/logger');
const FORCE_SHUTDOWN_TIMEOUT = 10000;

module.exports.cleanup = async (exitCode) => {
  Logger.log(`Kill signal ${exitCode} received, shutting down ${process.env.BOT_NAME}...`);
  setTimeout(() => {
    Logger.warn('Can\'t clean up gracefully, forcefully shutting down the application');
    process.exit(1);
  }, FORCE_SHUTDOWN_TIMEOUT);

  // Some cleanup here if necessary

  Logger.log(`${process.env.BOT_NAME} is closed.`);
  process.exit(0);
};