const {bot} = require('../config');
const {sendMessage} = require('../utils/telegramApiHelper');

module.exports.register = () => {
  bot.on('new_chat_members', async (ctx) => {
    sendMessage(ctx, `Hello ${ctx.message.from.username}!`, 'hello');
  });
};