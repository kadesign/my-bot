const Messages = require('./messages');

const {bot, botCommands} = require('../config');
const {sendMessage} = require('../utils/telegramApiHelper');

module.exports.register = () => {
  bot.command(botCommands.help, ctx => {
    sendMessage(ctx, Messages.commands.help, 'commands.help', true)
  });
};