const Logger = require('../utils/logger');
const Commands = require('./commands');
// const Listeners = require('./listeners');
const PreShutdown = require('./shutdownCleanup');

const {to} = require('../utils/helper');
const {bot, botCommands} = require('../config');

module.exports.initialize = async function () {
  let err, botInfo, startTime, endTime;

  Logger.log(process.env.BOT_NAME + ' started');
  Logger.log(`Current version: ${process.env.npm_package_version}`);
  if (process.env.NODE_ENV) Logger.log(`Application is running in ${process.env.NODE_ENV} mode`);

  Logger.log('Connectivity check started');

  startTime = new Date();
  [err, botInfo] = await to(bot.telegram.getMe());
  if (err) {
    if (err.code === 'ENOTFOUND' || err.code === 'ECONNRESET') throw new Error('Telegram API error: Not available');
    if (err.code === 401) throw new Error('Telegram API error: Invalid token');
    throw new Error('Telegram API error: ' + err.message);
  }
  if (botInfo) {
    endTime = new Date();
    Logger.log('Telegram API connection OK, ping: ' + (endTime - startTime).toString() + 'ms');
    if (process.env.BOT_NAME !== botInfo.username) throw new Error('Telegram API error: Wrong bot name configured');
    Logger.log('Telegram API token check OK');
  }

  Logger.log('Connectivity check finished');

  Logger.log('Configuring middlewares...');
  bot.use(async (ctx, next) => {
    if (ctx.updateType === 'message' && ctx.updateSubTypes.indexOf('text') >= 0) {
      const text = ctx.update.message.text.toLowerCase();

      if (text.startsWith('/') && /[A-Za-z]/.test(text.substr(1,1))) {
        const extractedCommand = text.split(' ')[0];
        const regex = new RegExp('^\\/([^\\s@]+)@' + process.env.BOT_NAME.toLowerCase());
        const match = extractedCommand.match(regex);
        const checkedCommand = (match && match[1]) ? match[1] : extractedCommand.substr(1);

        if (Object.values(botCommands).indexOf(checkedCommand) > -1) {
          Logger.log(`Chat (TgID: ${ctx.message.chat.id}): +${checkedCommand}`);
          await next();
          Logger.log(`Chat (TgID: ${ctx.message.chat.id}): -${checkedCommand}`);
        } else {
          Logger.warn(`Chat (TgID: ${ctx.message.chat.id}): unknown command "${checkedCommand}" ignored`)
        }
      }
    }
  });

  Logger.log('Registering commands...');
  Commands.register();

  /*
    Register listeners if necessary
   */
  // Logger.log('Registering listeners...');
  // Listeners.register();

  Logger.log('Registering pre-shutdown cleanup...');
  process.on('SIGTERM', PreShutdown.cleanup);
  process.on('SIGINT', PreShutdown.cleanup);

  Logger.log('Ready.');
};