require('dotenv').config();

// External libs
if (process.env.SENTRY_DSN) {
  const Sentry = require('@sentry/node');
  Sentry.init({
    environment: process.env.NODE_ENV ? process.env.NODE_ENV : undefined
  });
}

// Internal libs
const Logger = require('./utils/logger');
const {to} = require('./utils/helper');

// Components
const Initializer = require('./components/initializer');

// Bot initialization & commands
const {bot} = require('./config');

// Main workflow
async function main() {
  let [err] = await to(Initializer.initialize());
  if (err) throw err;

  bot.startPolling();
}

main().catch(err => {
  Logger.error(err);
});