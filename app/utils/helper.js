module.exports.to = function (promise) {
  return promise.then(data => {
    return [null, data];
  })
    .catch(err => [err]);
};

module.exports.getRandomIndex = arrayLength => {
  return Math.floor(Math.random() * arrayLength);
};

module.exports.getRandomBetween = (a, b) => {
  return a + Math.floor(Math.random() * b);
};

// Use if markdown === true
module.exports.fixUsername = username => {
  if (username.indexOf('_') >= 0) {
    username = username.replace(/_/g, '\\_');
  }

  return username;
};

module.exports.isObjectEmpty = obj => Object.keys(obj).length === 0 && obj.constructor === Object;

/*
  Fisher-Yates shuffle algorithm
  https://stackoverflow.com/a/6274381
 */
module.exports.shuffleArray = array => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};